const bodyParser = require('body-parser');
const express = require('express');
const app = express();

const port = process.env.PORT || 1313;

const highlighter = require('./highlighter');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers",
             "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

const router = express.Router();

router.get('/', (req, res) => {
  res.json({
    message: 'Welcome to the code highlighter!!',
  });
});

router.route('/test').get((req, res) => {
  const code = `
  import cgitb
  cgitb.enable()
  
  #print neccassary headers
  print("Content-Type: text/html")
  print()  
`;
const htmlCode = `
<html>
<body id='as'>
<img src="something.jpg">
<h1>something</h1>
<i>italic text</i>
<!-- this is a comment -->
</body>
</html>
`;
const cssCode = `
.dss {
   width: 213px;
}

#something {
   height: 213px;
}

body {
   margin: auto;
}
`;
const pythonCode = `
import cgitb
cgitb.enable()

#print necessary headers
print("Content-Type: text/html")
print()

while (i > 3):
   print(i)
   i++;

`;

  html = highlighter.init(htmlCode, 'markup', 'json', "Hopscotch");
  html.then((jsonoutput) => res.send(jsonoutput));
});

router.route('/highlight').post((req, res) => {
  const code = req.body.code;
  const lang = req.body.lang;
  const outputFormat = req.body.outputFormat || 'json';
  const cssTheme = req.body.theme;

  if( !code ) {
    res.json({
      'message':'well well well, you didn\'t send any code, what am I'+
                ' supposed to highlight here?',
      'error': true,
    });
  }

  if (outputFormat != 'html' && outputFormat != 'json') {
    res.json({
      'message':'It\'s not you it\'s me, I can\'t give output in any '+
                'other format except html or json',
      'error': true,
    });
  }

  const output = highlighter.init(code, lang, outputFormat, cssTheme);
  output.then((data) => {
    res.json({
      'message':'You are a good guy, Here\'s your styled code',
      'error': false,
      'output': data,
    });
  });
});

router.route('/getThemes').get((req, res) => {
  const list = highlighter.themes();
  if (list) {
   res.json({
      'message':'Your wish is my command, here\'s the list of themes',
      'error': false,
      'output': list,
    });
  } else {
    res.json({
      'message':'I am so sorry to fail you, but I just can\'t get the list'+
                ' at this moment, maybe try again in some time.',
      'error': true,
    });
  }
});

app.use('/api', router);

app.listen(port);
console.log('Let there be code highlighting at port ' + port);
