const prismjs = require("prismjs");
const loadLanguages = require("prismjs/components/");
const { JSDOM } = require("jsdom");
const colorString = require("color-string");
const colorConvert = require("color-convert");

const themes = require("./themes");

let result = [];

const highlight = (code, lang, outputFormat, cssTheme) => {
  result = [];
  loadLanguages([lang]);
  const codehtml = Prism.highlight(code, Prism.languages[lang], lang);
  const css = themes[cssTheme] || themes.atom_dark;

  const html = `<html><head><link rel='stylesheet' href='${css}'></head>
                <body><div id="highlighted-code">${codehtml}</div></body></html>`;

  const { window } = new JSDOM(html, {
    resources: "usable",
    pretendToBeVisual: true
  });

  const getResult = new Promise((resolve, reject) => {
    window.onload = function() {
      const container = window.document.getElementById("highlighted-code");
      parseElements(container.childNodes, window);
      if (outputFormat === "json") {
        resolve(JSON.stringify(result));
      } else {
        resolve(html);
      }
    };
  });
  return getResult;
};

const parseElements = (children, window) => {
  children.forEach(el => {
    if(el.nodeType == 3) {
      const parent = el.parentNode;
      if (parent.id == 'highlighted-code') {
        result.push([el.textContent, 'default']);
      } else {
        var color = window.getComputedStyle(parent).color;
        if(color) {
          result.push([el.textContent, getHex(color)]);
        } else {
          result.push([el.textContent, 'default']);
        }
      }
    } else if(el.children.length == 0) {
      var color = window.getComputedStyle(el).color;
      result.push([el.textContent, getHex(color)]);
    } else if (el.children.length) {
      parseElements(el.childNodes, window);
    }
  }); 
}

const getHex = color => {
  let hex;
  if (color.includes("rgb")) {
    const rgbColor = colorString.get(color);
    hex = "#" + colorConvert.rgb.hex(rgbColor.value);
  } else if (color.includes("hsl")) {
    const hslValues = colorString.get(color);
    const rgbColor = colorConvert.hsl.rgb(hslValues.value);
    hex = "#" + colorConvert.rgb.hex(rgbColor);
  }
  return hex;
};

const highlighter = {
  init: highlight,
  themes: () => Object.keys(themes)
};

module.exports = highlighter;
